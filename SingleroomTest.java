import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SingleroomTest {
    private Singleroom classOfSingleRoom;

    @Before
    public void initializeAfterEachTest(){

        this.classOfSingleRoom = new Singleroom("Nandy","9994429199","Male");

    }

    @Test
    public void correctInputTesting() {

        String expectedResult = "Nandy" ;

        String actualResult = classOfSingleRoom.name;

        assertEquals("Checking the name",expectedResult,actualResult);
    }

    @Test
    public void correctInputTesting2() {

        String expectedResult = "9994429199" ;

        String actualResult = classOfSingleRoom.contact;

        assertTrue("Checking the contact",expectedResult==actualResult);
    }

    @Test
    public void correctInputTesting3() {

        String expectedResult = "Male" ;

        String actualResult = classOfSingleRoom.gender;

        assertTrue("Checking the gender",expectedResult==actualResult);
    }

    @Test
    public void wrongInputTesting() {

        classOfSingleRoom = new Singleroom(null,"9994429199","Male");

        String actualResult = classOfSingleRoom.name;

        assertNull("Checking the null",actualResult);
    }

    @Test
    public void noInputTesting() {

        classOfSingleRoom = new Singleroom();

        String expectedResult = "" ;

        String actualResult = classOfSingleRoom.name;

        assertEquals("Checking the output without passing name",expectedResult,actualResult);
    }

    @Test
    public void nullPointerException() {

        assertThrows("Not assigning constructor",NullPointerException.class, () -> {
            classOfSingleRoom = null;
            System.out.println(classOfSingleRoom.name);
        });

    }

}