import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class holderTest {

    private holder classOfholder;

    @Before
    public void initializeBeforeTest() {
        classOfholder = new holder();
    }

    @Test
    public void checkingAvailbiltyOfRooms1() {

        classOfholder = new holder();

        int expectedResult = 20;

        int actualResult = classOfholder.deluxe_singleerrom.length;

        assertEquals("Checking total Number of deluxe single rooms",expectedResult,actualResult);
    }

    @Test
    public void checkingAvailbiltyOfRooms2() {

        classOfholder = new holder();

        int expectedResult = 20;

        int actualResult = classOfholder.deluxe_doublerrom.length;

        assertEquals("Checking total Number of deluxe double rooms",expectedResult,actualResult);
    }

    @Test
    public void checkingAvailbiltyOfRooms3() {

        classOfholder = new holder();

        int expectedResult = 10;

        int actualResult = classOfholder.luxury_singleerrom.length;

        assertEquals("Checking total Number of luxury single rooms",expectedResult,actualResult);
    }

    @Test
    public void checkingAvailbiltyOfRooms4() {

        classOfholder = new holder();

        int expectedResult = 10;

        int actualResult = classOfholder.luxury_doublerrom.length;

        assertEquals("Checking total Number of luxury double rooms",expectedResult,actualResult);
    }

    @Test
    public void nullPointerException() {

        assertThrows("Not assigning constructor", NullPointerException.class, () -> {

            classOfholder = null;

            System.out.println(classOfholder.deluxe_doublerrom.length);
        });
    }
}