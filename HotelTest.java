import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class HotelTest {
    
    private Hotel classOfHotel;
    
    @Before
    public void initializeBefore(){

        classOfHotel = new Hotel();

        classOfHotel.hotel_ob.deluxe_singleerrom[0] = new Singleroom("Nandy", "9994429199","Male");

    }

    @Test
    public void checkFromBackup() {

        String expectedResult = "Nandy" ;

        String actualResult = classOfHotel.hotel_ob.deluxe_singleerrom[0].name;

        assertEquals("Checking hotel room details",expectedResult,actualResult);

    }

    @Test
    public void checkingRoomWhichIsNA() {

        assertThrows("Room Empty", ArrayIndexOutOfBoundsException.class, () -> {

            classOfHotel.hotel_ob.deluxe_singleerrom[30] = new Singleroom("Nandy", "9994429199","Male");

        });

    }

    @Test
    public void checkingForNull() {

        classOfHotel.hotel_ob.deluxe_doublerrom[0] = new Doubleroom("Nandy2","9994429999","Male",null,"Nandy3","Male");

        String actualResult = classOfHotel.hotel_ob.deluxe_doublerrom[0].name2;

        assertNull("Checking for null",actualResult);

    }



    @Test
    public void checkDetailsOfEmptyRooms() {

        assertThrows("Room Empty", NullPointerException.class, () -> {

            String actualResult = classOfHotel.hotel_ob.deluxe_singleerrom[1].name;

        });

    }
}