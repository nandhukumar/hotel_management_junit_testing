import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FoodTest {

    private Food classOfFood;

    @Before
    public void initializeBeforeTest() {
        classOfFood = new Food(2, 2);
    }

    @Test
    public void correctInputTesting() {

        classOfFood = new Food(4, 2);

        float expectedResult = 60;

        float actualResult = classOfFood.price;

        assertEquals(expectedResult, actualResult, 0);
    }

    @Test
    public void wrongInputTesting() {

        classOfFood = new Food(5, 2);

        float expectedResult = 0;

        float actualResult = classOfFood.price;

        assertEquals(expectedResult, actualResult, 0);
    }

    @Test
    public void nullPointerException() {

        assertThrows("Not assigning constructor", NullPointerException.class, () -> {

            classOfFood = null;

            System.out.println(classOfFood.price);
        });

    }
}