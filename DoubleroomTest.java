import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleroomTest {

    private Doubleroom classOfDoubleroom;

    @Before
    public void initializeAfterEachTest(){

        this.classOfDoubleroom = new Doubleroom("Nandy","9994429199","Male","Nandhu","9994429999","Male");

    }

    @Test
    public void correctInputTesting() {

        String expectedResult = "Nandy" ;

        String actualResult = classOfDoubleroom.name;

        assertEquals("Checking the name",expectedResult,actualResult);
    }

    @Test
    public void correctInputTesting2() {

        String expectedResult = "9994429199" ;

        String actualResult = classOfDoubleroom.contact;

        assertTrue("Checking the contact",expectedResult == actualResult);
    }

    @Test
    public void correctInputTesting3() {

        String expectedResult = "Male" ;

        String actualResult = classOfDoubleroom.gender;

        assertTrue("Checking the gender",expectedResult == actualResult);
    }

    @Test
    public void wrongInputTesting() {

        classOfDoubleroom = new Doubleroom(null,"9994429199","Male","Nandhu","9994429999","Male");

        String actualResult = classOfDoubleroom.name;

        assertNull("Checking the null",actualResult);
    }

    @Test
    public void noInputTesting() {

        classOfDoubleroom = new Doubleroom();

        String expectedResult = "" ;

        String actualResult = classOfDoubleroom.name;

        assertEquals("Checking the output without passing name",expectedResult,actualResult);
    }

    @Test
    public void nullPointerException() {

        assertThrows("Not assigning constructor",NullPointerException.class, () -> {
            classOfDoubleroom = null;
            System.out.println(classOfDoubleroom.name);
        });

    }


}