import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NotAvailableTest {

    private NotAvailable classOfNotAvailable;

    @Before
    public void initializeBeforeTest() {
        classOfNotAvailable = new NotAvailable();
    }

    @Test
    public void correctInputTesting() {

        String expectedResult = "Not Available !";

        String actualResult = classOfNotAvailable.toString();

        assertEquals("Checking whether rooms are available", expectedResult, actualResult);
    }

    @Test
    public void nullPointerException() {

        assertThrows("Not assigning constructor", NullPointerException.class, () -> {

            classOfNotAvailable = null;

            System.out.println(classOfNotAvailable.toString());
        });

    }
}