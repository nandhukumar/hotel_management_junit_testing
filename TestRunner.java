import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(JSuiteClass.class);

        for(Failure failure : result.getFailures()){
            System.out.println(failure.getTrace());
            System.out.println();
            System.out.println("Failed to pass the testCases");

        }

        if(result.wasSuccessful()){
            System.out.println("Code passed in all testCases");
        }
    }
}

